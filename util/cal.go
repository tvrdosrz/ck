package util

import "time"

var (
	SelTime time.Time
	Today   time.Time
)

func Date(year int, month time.Month, day int) time.Time {
	return time.Date(year, month, day, 0, 0, 0, 0, time.Local)
}
