package drw

import (
	"unicode/utf8"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/tvrdosrz/ck/util"
)

var (
	months = [...]string{"", "Јануар", "Фебруар", "Март", "Април", "Мај", "Јун",
		"Јул", "Август", "Септембар", "Октобар", "Новембар", "Децембар"}
	weekdays = [...]string{"Недеља", "Понедељак", "Уторак", "Среда", "Четвртак",
		"Петак", "Субота"}
)

var (
	DefStyle   tcell.Style
	TodayStyle tcell.Style
	SelStyle   tcell.Style
)

func centeredText(s string, x, y, width int, scr tcell.Screen, style tcell.Style) {
	start := x + util.Max((width-utf8.RuneCountInString(s))/2, 0)
	i := 0
	for ; x+i < start; i++ {
		scr.SetContent(x+i, y, ' ', nil, style)
	}
	for _, char := range s {
		if i >= width {
			break
		}

		scr.SetContent(x+i, y, char, nil, style)
		i++
	}
	for ; x+i < start; i++ {
		scr.SetContent(x+i, y, ' ', nil, style)
	}
}

func wrappedText(s string, scr tcell.Screen, style tcell.Style) {
	x := 0
	y := 0
	w, h := scr.Size()
	for _, char := range s {
		if x >= w {
			if y >= h {
				break
			}
			x = 0
			y++
		}
		scr.SetContent(x, y, char, nil, style)
		x++
	}
}
