package main

import (
	"log"
	"os"
	"time"

	"github.com/gdamore/tcell/v2"

	"gitlab.com/tvrdosrz/ck/drw"
	"gitlab.com/tvrdosrz/ck/util"
)

func main() {
	util.Today = time.Now()
	util.SelTime = util.Today
	var err error
	drw.Wall, err = tcell.NewScreen()
	if err != nil {
		log.Fatalf("%+v", err)
	}
	if err := drw.Wall.Init(); err != nil {
		log.Fatalf("%+v", err)
	}
	drw.DefStyle = tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.ColorReset)
	drw.TodayStyle = tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.GetColor("blue")).Bold(true).Reverse(true)
	drw.SelStyle = tcell.StyleDefault.Background(tcell.ColorReset).Foreground(tcell.GetColor("green")).Bold(true)

	drw.Wall.SetStyle(drw.DefStyle)
	drw.Wall.Clear()
	drw.DrawWall()

	quit := func() {
		drw.Wall.Fini()
		os.Exit(0)
	}
	for {
		// Update screen
		drw.Wall.Show()

		// Poll event
		ev := drw.Wall.PollEvent()

		// Process event
		switch ev := ev.(type) {
		case *tcell.EventResize:
		case *tcell.EventKey:
			if ev.Key() == tcell.KeyEscape || ev.Key() == tcell.KeyCtrlC ||
				ev.Rune() == 'Q' || ev.Rune() == 'q' {
				quit()
			}
			switch ev.Rune() {
			case 'h':
				drw.MoveLeft()
			case 'j':
				drw.MoveDown()
			case 'k':
				drw.MoveUp()
			case 'l':
				drw.MoveRight()
			}
		}

		drw.DrawWall()
		drw.Wall.Sync()
	}
}
